#ifndef VISUAL_TESTS_IMGFILE_HPP
#define VISUAL_TESTS_IMGFILE_HPP

#include <iostream>
#include <vector>

#include "canvas.hpp"
#include "circle.hpp"
#include "dot.hpp"
#include "line.hpp"
#include "rgb_color.hpp"
#include "shaders_primitive.hpp"
#include "shaders_vertex.hpp"
#include "triangle.hpp"

namespace tests {

template <size_t width, size_t height, size_t display_size>
class visual_tests_imgfile {
 public:
  static void run() {
    test_1("01_image_dot.ppx");
    test_2("02_image_lines.ppx");
    test_3("03_image_cross_lines.ppx");
    test_4("04_image_triangle.ppx");
    test_5("05_image_circle.ppx");
    test_6("06_image_invert_primitive_border.ppx");
    test_7("07_image_fill_primitive_with_color.ppx");
    test_8("08_image_invert_color_inside_primitive_borders_1.ppx");
    test_9("09_image_invert_color_inside_primitive_borders_2.ppx");

    test_clipping_1("10_image_clipping_rectangle.ppx");
  }

 private:
  static void test_clipping_1(const char* filepath) {
    using namespace simple_drawing;

    primitive_canvas canvas(height, width);

    color selected_color = color::_FIRST_;
    line current_line;

    // init clipping window

    // apply clipping window

    // TODO: pixbuffer must draw primitives!

    for (coord i = 1; i < signed_distance(width - 1); ++i) {
      selected_color = color(selected_color + 1);

      if (selected_color == color::_LAST_)
        selected_color = color(color::_FIRST_ + 1);

      current_line = line({i, 1}, {i, coord(height - 10)}, selected_color);

      canvas.draw(current_line);
    }

    canvas.save<display_size>(filepath);

    std::cout << filepath << " is ready" << std::endl;
  }

  static void test_1(const char* filepath) {
    using namespace simple_drawing;

    primitive_canvas canvas(height, width);

    auto dt = dot({20, 20}, color::DarkOrange1B);

    canvas.draw(dt);

    canvas.save<display_size>(filepath);

    std::cout << filepath << " is ready" << std::endl;
  }

  static void test_2(std::string filepath) {
    using namespace simple_drawing;

    primitive_canvas canvas(height, width);

    auto ln1 = line({100, 100}, {150, 150}, color::Red);
    canvas.draw(ln1);
    auto ln3 = line({100, 100}, {150, 100}, color::Yellow);
    canvas.draw(ln3);
    auto ln6 = line({100, 100}, {150, 50}, color::Indigo);
    canvas.draw(ln6);
    auto ln2 = line({100, 100}, {100, 150}, color::Orange);
    canvas.draw(ln2);
    auto ln5 = line({100, 100}, {100, 50}, color::Blue);
    canvas.draw(ln5);
    auto ln7 = line({100, 100}, {50, 150}, color::Purple);
    canvas.draw(ln7);
    auto ln4 = line({100, 100}, {50, 100}, color::Green);
    canvas.draw(ln4);
    auto ln8 = line({100, 100}, {50, 50}, color::Gray);
    canvas.draw(ln8);
    auto ln = line({100, 100}, {100, 100}, color::Black);
    canvas.draw(ln);

    auto ln9 = line({0, 0}, {190, 13}, color::Black);
    canvas.draw(ln9);
    auto ln10 = line({190, 14}, {0, 1}, color::Orange);
    canvas.draw(ln10);

    canvas.save<display_size>(filepath);

    std::cout << filepath << " is ready" << std::endl;
  }

  static void test_3(std::string filepath) {
    using namespace simple_drawing;

    primitive_canvas canvas(height, width);
    // NOTE: this method is useless in these tests but it has meaning if canvas
    // has a default color
    canvas.erase();

    auto lin1 = line({87, 87}, {177, 177}, color::Red);
    canvas.draw(lin1);
    auto lin3 = line({87, 87}, {177, 87}, color::Yellow);
    canvas.draw(lin3);
    auto lin6 = line({87, 87}, {177, 27}, color::Indigo);
    canvas.draw(lin6);
    auto lin2 = line({87, 87}, {87, 177}, color::Orange);
    canvas.draw(lin2);
    auto lin5 = line({87, 87}, {87, 27}, color::Blue);
    canvas.draw(lin5);
    auto lin7 = line({87, 87}, {27, 177}, color::Purple);
    canvas.draw(lin7);
    auto lin4 = line({87, 87}, {27, 87}, color::Green);
    canvas.draw(lin4);
    auto lin8 = line({87, 87}, {27, 27}, color::Gray);
    canvas.draw(lin8);
    auto lin = line({87, 87}, {87, 87}, color::Black);
    canvas.draw(lin);

    auto lin9 = line({0, 0}, {190, 13}, color::Black);
    canvas.draw(lin9);
    auto lin10 = line({190, 14}, {0, 1}, color::Orange);
    canvas.draw(lin10);

    auto lin11 = line({0, 13}, {190, 0}, color::Green_springB);
    canvas.draw(lin11);
    auto lin12 = line({190, 1}, {0, 14}, color::Khaki1_l);
    canvas.draw(lin12);

    auto lin13 = line({0, 0}, {13, 190}, color::Lime);
    canvas.draw(lin13);
    auto lin14 = line({14, 190}, {1, 0}, color::Red);
    canvas.draw(lin14);

    auto lin15 = line({13, 0}, {0, 190}, color::Cyan);
    canvas.draw(lin15);
    auto lin16 = line({1, 190}, {14, 0}, color::Teal);
    canvas.draw(lin16);

    canvas.save<display_size>(filepath);

    std::cout << filepath << " is ready" << std::endl;
  }

  static void test_4(std::string filepath) {
    using namespace simple_drawing;

    primitive_canvas canvas(height, width);

    canvas.erase();

    auto tr1 = triangle({170, 170}, {10, 170}, {25, 10}, color::Navy);
    canvas.draw(tr1);

    canvas.save<display_size>(filepath);

    std::cout << filepath << " is ready" << std::endl;
  }

  static void test_5(std::string filepath) {
    using namespace simple_drawing;

    primitive_canvas canvas(height, width);

    canvas.erase();

    auto c1 = circle({100, 100}, {50, 50}, color::Navy);
    canvas.draw(c1);

    canvas.save<display_size>(filepath);

    std::cout << filepath << " is ready" << std::endl;
  }

  static void test_6(std::string filepath) {
    using namespace simple_drawing;

    primitive_canvas canvas(height, width);

    canvas.erase();

    auto c1 = circle({100, 100}, 10, color::Red);
    canvas.draw(c1);

    auto c2 = circle({100, 100}, {50, 50}, color::Red);
    canvas.applyShader(shaders::primitive::invert_border_color, c2);

    canvas.draw(c2);

    canvas.save<display_size>(filepath);

    std::cout << filepath << " is ready" << std::endl;
  }

  static void test_7(std::string filepath) {
    using namespace simple_drawing;

    primitive_canvas canvas(height, width);

    canvas.erase();

    auto tr2 = triangle({10, 170}, {25, 10}, {170, 170}, color::Navy);
    canvas.draw(tr2);

    auto v = new std::vector<pvertex>;
    v->push_back({10, 170});
    v->push_back({25, 10});
    v->push_back({170, 170});
    canvas.applyShader(shaders::vertex::fill_color_3v, *v, color::Red);

    canvas.save<display_size>(filepath);

    std::cout << filepath << " is ready" << std::endl;
  }

  static void test_8(std::string filepath) {
    using namespace simple_drawing;

    primitive_canvas canvas(height, width);

    canvas.erase();

    auto tr3 = triangle({10, 170}, {25, 10}, {170, 170}, color::Navy);
    canvas.draw(tr3);

    auto v = new std::vector<pvertex>;
    v->push_back({10, 170});
    v->push_back({25, 10});
    v->push_back({170, 170});

    canvas.applyShader(shaders::vertex::fill_color_3v, *v, color::Red);
    canvas.applyShader(shaders::vertex::invert_color_3v, *v);

    canvas.save<display_size>(filepath);

    std::cout << filepath << " is ready" << std::endl;
  }

  static void test_9(std::string filepath) {
    using namespace simple_drawing;

    primitive_canvas canvas(height, width);

    canvas.erase();

    auto tr3 = triangle({10, 170}, {25, 10}, {170, 170}, color::Navy);
    canvas.draw(tr3);

    auto v = new std::vector<pvertex>;
    v->push_back({10, 170});
    v->push_back({25, 10});
    v->push_back({170, 170});

    canvas.applyShader(shaders::vertex::fill_color_3v, *v, color::Red);

    line crossing_line_1_({1, 1}, {150, 150}, color::Yellow);
    line crossing_line_2_({114, 150}, {12, 1}, color::Blue);
    line crossing_line_3_({0, 50}, {75, 75}, color::Olive);

    canvas.draw(crossing_line_1_);
    canvas.draw(crossing_line_2_);
    canvas.draw(crossing_line_3_);

    auto v2 = new std::vector<pvertex>;
    v2->push_back({10, 150});
    v2->push_back({25, 45});
    v2->push_back({170, 160});

    canvas.applyShader(shaders::vertex::invert_color_3v, *v2);

    canvas.save<display_size>(filepath);

    std::cout << filepath << " is ready" << std::endl;
  }
};

}  // namespace tests

#endif  // VISUAL_TESTS_IMGFILE_HPP
