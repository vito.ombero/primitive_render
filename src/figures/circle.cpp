#include "circle.hpp"
#include "algorithmes.hpp"

namespace simple_drawing {

circle::~circle() { delete r_buf; }

circle::circle() : center({3, 3}), left_point_at_border({1, 1}) {
  setColor(color::Black);
  set_offset(center, left_point_at_border);
}

circle::circle(point c, point l, color co = color::Black)
    : center(c), left_point_at_border(l) {
  setColor(co);
  set_offset(c, l);
}

circle::circle(point c, signed_distance r, color co = color::Black)
    : center(c) {
  signed_distance d = (r == 0) ? 1 : r;
  coord one = c.x + d - 1;
  coord two = c.y + d - 1;
  r_buf = new signed_distance(r);
  left_point_at_border = point{one, two};
  setColor(co);
}

void circle::set_offset(point center, point len) noexcept {
  signed_distance d =
      (r_buf == nullptr) ? algorithmes::calc_distance(center, len) : *r_buf;
  // assert(center.x + 1 - d >= 0); always true
  // assert(center.y + 1 - d >= 0); always true
  coord q = center.x - d + 1;
  coord w = center.y - d + 1;
  coord e = center.x + d - 1;
  coord r = center.y + d - 1;
  offset_.left_upper = point{q, w};
  offset_.right_lower = point{e, r};
}

int_least8_t circle::image_self_into(std::vector<point>& positions) noexcept {
  if (getColor() != color::No) {
    distance count{0};
    distance d =
        (r_buf == nullptr)
            ? distance(algorithmes::calc_distance(center, left_point_at_border))
            : distance(*r_buf);
    algorithmes::cirle_positions(center, d, positions, count);
    set_pixels_total(count);
    positions.shrink_to_fit();
    return 1;
  } else {
    return 0;
  }
}

}  // namespace simple_drawing
