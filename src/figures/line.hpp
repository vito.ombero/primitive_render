#ifndef FIG_LINE_H
#define FIG_LINE_H

#include "drawable_primitive.hpp"
#include "sd_types.hpp"

namespace simple_drawing {

struct line final : drawable_primitive {
  line();

  line(point p1, point p2);

  line(point p1, point p2, color c);

  void set_offset(point l, point r) noexcept override;

  int_least8_t image_self_into(std::vector<point>& positions) override;

 private:
  point p1;
  point p2;
};

}  // namespace simple_drawing
#endif  // FIG_LINE_H
