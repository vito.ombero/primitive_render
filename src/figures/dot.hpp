#ifndef DOT_HPP
#define DOT_HPP

#include "drawable_primitive.hpp"
#include "sd_types.hpp"

namespace simple_drawing {

struct dot : drawable_primitive {
  int_least8_t image_self_into(std::vector<point>& positions) override;

  dot();
  dot(coord x, coord y);
  dot(point p_, color c);

  void set_offset(point l, point r) noexcept override;

 private:
  point p;
};

}  // namespace simple_drawing
#endif  // DOT_HPP
