#include "triangle.hpp"
#include "algorithmes.hpp"  // sick

namespace simple_drawing {

int_least8_t triangle::image_self_into(std::vector<point> &positions) {
  if (getColor() != color::No) {
    distance count{0};
    algorithmes::line_positions(c, a, positions, count);
    algorithmes::line_positions(a, b, positions, count);
    algorithmes::line_positions(b, c, positions, count);

    set_pixels_total(count);
    positions.shrink_to_fit();
    return 1;
  } else {
    return 0;
  }
}

triangle::triangle() : a({0, 0}), b({2, 0}), c({0, 2}) {
  setColor(color::Black);
  set_offset(a, {2, 2});
}

triangle::triangle(point a, point b, point c) : a(a), b(b), c(c) {
  setColor(color::Black);

  set_offset(algorithmes::make_remotest_point(a, b, c, false),
             algorithmes::make_remotest_point(a, b, c, true));
}

triangle::triangle(point a, point b, point c, color co) : triangle(a, b, c) {
  setColor(co);
}

void triangle::set_offset(point l, point r) noexcept {
  offset_.left_upper = l;
  offset_.right_lower = r;
}

}  // namespace simple_drawing
