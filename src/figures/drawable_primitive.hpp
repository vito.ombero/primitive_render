
#ifndef PIXALIZABLE_H_
#define PIXALIZABLE_H_

#include <cassert>
#include <map>
#include <memory>
#include <vector>

#include "rgb_color.hpp"
#include "sd_types.hpp"

namespace simple_drawing {

struct drawable_primitive {
 public:
  void setColor(color c);
  color getColor() const;
  point get_left_upper_offset() const;
  point get_right_lower_offset() const;
  distance get_pixels_total() const;

  virtual int_least8_t image_self_into(std::vector<point>& positions) = 0;
  virtual void set_offset(point luop, point rdop) noexcept = 0;

 private:
  color col = color::White;
  distance pixelsTotal;
  // TODO: make private list<vertex> + methods to implement by children

 protected:
  drawable_primitive();
  virtual ~drawable_primitive();

  offset offset_;
  void set_pixels_total(distance n);
};

}  // namespace simple_drawing

#endif /* PIXALIZABLE_H_ */
