#ifndef FIG_TRIANGLE_H
#define FIG_TRIANGLE_H

#include "drawable_primitive.hpp"
#include "sd_types.hpp"

namespace simple_drawing {

struct triangle final : drawable_primitive {
  triangle();

  triangle(point a, point b, point c);

  triangle(point a, point b, point c, color);

  void set_offset(point l, point r) noexcept override;

  int_least8_t image_self_into(std::vector<point>& positions) override;

 private:
  point a;
  point b;
  point c;
};

}  // namespace simple_drawing

#endif  // FIG_TRIANGLE_H
