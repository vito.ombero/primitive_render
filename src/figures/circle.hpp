#ifndef FIG_CIRCLE_HPP
#define FIG_CIRCLE_HPP

#include "drawable_primitive.hpp"
#include "sd_types.hpp"

namespace simple_drawing {

struct circle final : drawable_primitive {
 public:
  ~circle() override;

  circle();

  circle(point c, point l, color co);

  circle(point c, signed_distance r, color co);

  void set_offset(point center, point len) noexcept override;

  int_least8_t image_self_into(std::vector<point>& positions) noexcept override;

 private:
  point center;
  point left_point_at_border;
  signed_distance* r_buf = nullptr;
};

}  // namespace simple_drawing
#endif  // FIG_CIRCLE_HPP
