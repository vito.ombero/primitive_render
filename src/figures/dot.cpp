#include "dot.hpp"
#include "algorithmes.hpp"

namespace simple_drawing {

int_least8_t dot::image_self_into(std::vector<point>& positions) {
  if (getColor() != color::No) {
    positions.push_back(p);
    set_pixels_total(1);
    return 1;
  } else {
    return 0;
  }
}

dot::dot() : p({0, 0}) {
  setColor(color::Black);
  set_offset({0, 0}, {0, 0});
}

dot::dot(coord x, coord y) : p({x, y}) {
  setColor(color::Black);
  set_offset(p, p);
}

dot::dot(point p_, color c) : p(p_) {
  setColor(c);
  set_offset(p, p);
}

void dot::set_offset(point l, point r) noexcept {
  offset_.right_lower = {l.x, l.y};
  offset_.left_upper = {r.x, r.y};
}

}  // namespace simple_drawing
