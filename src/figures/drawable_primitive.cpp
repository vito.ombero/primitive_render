/*
 * pixalizable.cpp
 *
 *  Created on: Oct 6, 2018
 *      Author: vito
 */

#include <map>

#include "drawable_primitive.hpp"
#include "rgb_color.hpp"
#include "sd_types.hpp"

namespace simple_drawing {

drawable_primitive::~drawable_primitive() {}

void drawable_primitive::setColor(color c) { col = c; }
color drawable_primitive::getColor() const { return col; }
point drawable_primitive::get_left_upper_offset() const {
  return offset_.left_upper;
}
point drawable_primitive::get_right_lower_offset() const {
  return offset_.right_lower;
}
distance drawable_primitive::get_pixels_total() const { return pixelsTotal; }

drawable_primitive::drawable_primitive()
    : pixelsTotal(0), offset_({0, 0}, {0, 0}) {}

void drawable_primitive::set_pixels_total(distance n) { pixelsTotal = n; }

}  // namespace simple_drawing
