#include "line.hpp"
#include "algorithmes.hpp"  // sick

namespace simple_drawing {
int_least8_t line::image_self_into(std::vector<point> &positions) {
  if (getColor() != color::No) {
    distance count{0};
    algorithmes::line_positions(p1, p2, positions, count);
    set_pixels_total(count);
    positions.shrink_to_fit();
    return 1;
  } else {
    return 0;
  }
}

line::line() : p1({0, 0}), p2({1, 1}) {
  setColor(color::Black);
  set_offset(p1, p2);
}

line::line(point p1, point p2) : p1(p1), p2(p2) {
  setColor(color::Black);
  set_offset(p1, p2);
}

line::line(point p1, point p2, color c) : p1(p1), p2(p2) {
  setColor(c);
  set_offset(p1, p2);
}

void line::set_offset(point l, point r) noexcept {
  coord lx{l.x};
  coord ly{l.y};
  coord hx{r.x};
  coord hy{r.y};
  if (lx > hx) {
    lx = r.x;
    hx = l.x;
  }
  if (ly > hy) {
    ly = r.y;
    hy = l.x;
  }
  offset_.left_upper = {lx, ly};
  offset_.right_lower = {hx, hy};
}

}  // namespace simple_drawing
