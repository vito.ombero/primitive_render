#include <cstddef>

#include "GL/freeglut.h"
#include "visual_tests_glut_window.hpp"
#include "visual_tests_imgfile.hpp"

// ----------------------------------------------------------------------
// NOTE: width and height should be inside simple_drawing::size entity...
// ----------------------------------------------------------------------
// Test Settings
// ----------------------------------------------------------------------

constexpr size_t width = 300;
constexpr size_t height = 300;

constexpr size_t display_start_x = 1;
constexpr size_t display_start_y = 1;

template <size_t width, size_t height>
constexpr size_t const_mult() {
  return (width - display_start_x) * (height - display_start_y);
}

constexpr size_t display_size = const_mult<width, height>();

int main(int argc, char* argv[]) {
  using namespace tests;

  auto img_2d_tests = visual_tests_imgfile<width, height, display_size>();
  img_2d_tests.run();

  // gl windows go after file generation because there is no glut wrapper anmd
  // glut cycle manager
  // NOTE: don't resize windows because there is no opengl
  // context manager

  auto wtests =
      visual_tests_glut_window<width, height, display_size>(argc, argv);
  // never return from this
  wtests.run();

  return 0;
}
