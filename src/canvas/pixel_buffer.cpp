#include <cassert>
#include <vector>

#include "algorithmes.hpp"
#include "color_map.hpp"
#include "pixel_buffer.hpp"
#include "rgb_color.hpp"
#include "sd_types.hpp"

namespace simple_drawing {

pixel_buffer::pixel_buffer(distance width, distance height, color background) {
  if ((height > DISTANCE_MAX) || (width > DISTANCE_MAX)) {
    throw "Too big number for distance!";
  }

  /// width is always > height; pixelBuffer has no space orientation
  if (height < width) {
    h = signed_distance(width);
    w = signed_distance(height);
  } else {
    h = signed_distance(height);
    w = signed_distance(width);
  }

  pixels = new std::vector<rgb_color>();

  for (signed_distance i = 0; i < (h - 1) * (w - 1); ++i) {
    pixels->push_back(color_map[background]);
  }
}

int_least8_t pixel_buffer::update(drawable_primitive &primitive_,
                                  signed_distance prim_W,
                                  signed_distance prim_H) {
  auto primitive_points = new std::vector<point>(primitive_.get_pixels_total());

  // TODO: why does pixbuf care of prim invariant?
  // because of coord_to_index logic: hardcoded logic connection with
  // pixel_buffer container (1d vector)
  assert(primitive_points->size() <= distance(prim_W * prim_H));

  if (primitive_.image_self_into(*primitive_points)) {
    for (point tp : *primitive_points) {
      pixels->at(q1_coord(
          algorithmes::coords_to_index(tp.x, tp.y, distance(w), distance(h)))) =
          color_map.find(primitive_.getColor())->second;
    }
  }
  delete primitive_points;

  return 0;
}

distance pixel_buffer::get_height() const { return distance(h); }

distance pixel_buffer::get_width() const { return distance(w); }

pixel_buffer::~pixel_buffer() { delete pixels; }
}  // namespace simple_drawing
