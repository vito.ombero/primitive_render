#ifndef GSHADERS_HXX_
#define GSHADERS_HXX_

#include <algorithm>
#include <cmath>
#include <vector>

#include "sd_types.hpp"

#include "color_map.hpp"
#include "drawable_primitive.hpp"
#include "rgb_color.hpp"

namespace simple_drawing {
namespace shaders {
namespace primitive {

void invert_border_color(drawable_primitive& p);

}  // namespace primitive
}  // namespace shaders
}  // namespace simple_drawing
#endif /* GSHADERS_HXX_ */
