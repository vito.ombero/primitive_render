/*
 * primitive.h
 *
 *  Created on: Sep 29, 2018
 *      Author: vito
 */

#ifndef PRIMITIVERENDER_H_
#define PRIMITIVERENDER_H_

#include <cassert>
#include <iostream>
#include <memory>
#include <vector>

#include "algorithmes.hpp"
#include "dot.hpp"
#include "drawable_primitive.hpp"
#include "imgfile.hpp"
#include "pixel_buffer.hpp"
#include "sd_types.hpp"

namespace simple_drawing {

typedef void (*vertex_algorithm_c)(std::vector<pvertex>&,
                                   std::vector<rgb_color>&, distance, distance,
                                   color);
typedef void (*vertex_algorithm)(std::vector<pvertex>&, std::vector<rgb_color>&,
                                 distance, distance);
typedef void (*pixel_algorithm)(std::vector<point>&, std::vector<rgb_color>&);
typedef void (*algorithm)(simple_drawing::drawable_primitive&);
typedef void (*vshader)(vertex_algorithm_c);
typedef void (*pshader)(pixel_algorithm);
typedef void (*gshader)(algorithm, std::vector<rgb_color>&);

class primitive_canvas final {
 public:
  primitive_canvas(distance, distance, color = color::White);
  ~primitive_canvas();

  int_least8_t erase();

  template <size_t buffer_size>
  int_least8_t save(std::string filepath) {
    std::array<rgb_color, buffer_size> image;

    imprint_buffer(image);

    imgFile f(filepath);
    f.save_p6(image, get_width(), get_height());

    return 0;
  }

  template <size_t buffer_size>
  std::array<rgb_color, buffer_size> get_image() {
    std::array<rgb_color, buffer_size> image;

    imprint_buffer(image);

    return image;
  }

  int_least8_t applyShader(algorithm g, drawable_primitive& p);
  int_least8_t applyShader(pixel_algorithm p, std::vector<point>& borders);
  int_least8_t applyShader(vertex_algorithm_c v, std::vector<pvertex>& vv,
                           color);
  int_least8_t applyShader(vertex_algorithm v, std::vector<pvertex>& vv);

  template <size_t N>
  void imprint_buffer(std::array<rgb_color, N>& image) noexcept {
    for (long unsigned int i = 0; i < image.size(); ++i) {
      image[i] = pixel_buffer_ptr->pixels->at(i);
    }
  }

  int_least8_t draw(drawable_primitive& primitive_);

 private:
  primitive_canvas(const primitive_canvas&) = delete;
  primitive_canvas(primitive_canvas&&) = delete;
  primitive_canvas& operator=(const primitive_canvas&) = delete;
  primitive_canvas& operator=(const primitive_canvas&&) = delete;
  void* operator new(std::size_t, void*) = delete;

  pixel_buffer* pixel_buffer_ptr;

  distance get_height() const;
  distance get_width() const;
};
}  // namespace simple_drawing

#endif /* PRIMITIVERENDER_H_ */
