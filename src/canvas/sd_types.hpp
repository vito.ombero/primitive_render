#ifndef vitoBasicDrawingTYPES_HXX_
#define vitoBasicDrawingTYPES_HXX_

#include <map>

namespace simple_drawing {

typedef uint32_t q1_coord, distance;

#define COORD_MAX 800
#define DISTANCE_MAX 800
#define IMAGE_H_OR_W_MAX DISTANCE / 2

typedef int32_t coord, signed_distance;

// https://en.cppreference.com/w/cpp/types/byte ??
typedef unsigned char byte;

struct point {
  coord x;
  coord y;
};

struct offset {
  offset(point left_upper, point right_lower)
      : left_upper(left_upper), right_lower(right_lower) {}

  point left_upper;
  point right_lower;
};

typedef point pvertex;

/// rainbow colors: red/orange/yellow/green/blue/indigo/violet
enum color {
  No,
  Unnamed,

  _FIRST_,

  /// http://shallowsky.com/colormatch/index.php
  Black,          //#000000	(0,0,0)
  Navy,           //#000080	(0,0,128)
  Blue,           //#0000FF	(0,0,255)
  Green,          //#008000	(0,128,0)
  Teal,           //#008080	(0,128,128)
  Blue_light,     //####### 	(0,128,255)
  Lime,           //#00FF00	(0,255,0)
  Green_springB,  //#######	(0,255,128) Green_spring_is(0,255,127)
  Cyan,           //#00FFFF	(0,255,255)
  Indigo,         //#######	(75,0,130)
  Maroon,         //#800000	(128,0,0)
  Purple,         //#800000	(128,0,128)
  Purple_blue,    //#800000	(128,0,255)
  Olive,          //#808000	(128,128,0)
  Gray,           //#808080	(128,128,128)
  Sky_blue_dark,  //#######	(128,128,255)
  Silver,         //#C0C0C0	(192,192,192)
  Violet,         //####### 	(238,130,238)
  Red,            //#FF0000	(255,0,0)
  Fuchsia_dark,   //#FF0000	(255,0,128)
  Magenta,        //#FF00FF	(255,0,255)
  DarkOrange1B,   //#FF0000	(255,128,0) DarkOrange1_is(255,127,0)
  Orange,         //#######	(255,165,0)
  Coral_lightd,   //#FF0000	(255,128,128)
  Orchid1_l,      //#FF0000	(255,128,255)
  Yellow,         //#FFFF00	(255,255,0)
  Khaki1_l,       //#FFFF00	(255,255,128)
  White,          //#FFFFFF	(255,255,255)

  _LAST_
};

}  // namespace simple_drawing
#endif /* vitoBasicDrawingTYPES_HXX_ */
