/*
 * algorytms.cxx
 *
 *  Created on: Oct 5, 2018
 *      Author: vito
 */

#include <cmath>
#include <vector>

#include "algorithmes.hpp"

namespace simple_drawing {

// NOTE: refactor into 2 func for max and min coords and use bubble sort
// choose max or min coordinates by 0x and 0y
point algorithmes::make_remotest_point(point a, point b, point c,
                                       bool max /*or min?*/) {
  coord lu1{a.x}, lu2{a.y};
  if (max) {
    lu1 = b.x > lu1 ? b.x : lu1;
    lu1 = c.x > lu1 ? c.x : lu1;
    lu2 = b.y > lu1 ? b.y : lu2;
    lu2 = c.y > lu1 ? c.y : lu2;
  } else {
    lu1 = b.x < lu1 ? b.x : lu1;
    lu1 = c.x < lu1 ? c.x : lu1;
    lu2 = b.y < lu1 ? b.y : lu2;
    lu2 = c.y < lu1 ? c.y : lu2;
  }

  return point{lu1, lu2};
}

// save "empty" pixels of line (p1,p2) in vector
// current model has no pixel (as a term), there are colors and their positions
// in 1d container only
void algorithmes::line_positions(
    simple_drawing::point p1, simple_drawing::point p2,
    std::vector<simple_drawing::point>& positions_output,
    simple_drawing::distance& pixels_count_output) {
  coord x1 = p1.x;
  coord y1 = p1.y;
  coord x2 = p2.x;
  coord y2 = p2.y;

  // NOTE: NEVER DEFINE coord type for Cartesian coordinate system as uint EVEN
  // if you want to work in Quadrant I
  // Bresenham's line algorithm realization for uint coords is ugly, I saw it,
  // I'll never forget it, I'm going to live with it

  auto lineX = [&](coord x1, coord y1, coord x2, coord y2) {
    coord dx = x2 - x1;
    coord dy = y2 - y1;
    coord yi = 1;
    if (dy < 0) {
      yi = -1;
      dy = -dy;
    }
    coord D = 2 * dy - dx;
    coord y = y1;

    for (coord x = x1; x <= x2; ++x) {
      positions_output.push_back({x, y});
      pixels_count_output++;
      if (D > 0) {
        y += yi;
        D -= 2 * dx;
      }
      D += 2 * dy;
    }
  };

  auto lineY = [&](coord x1, coord y1, coord x2, coord y2) {
    coord dx = x2 - x1;
    coord dy = y2 - y1;
    coord xi = 1;
    if (dx < 0) {
      xi = -1;
      dx = -dx;
    }
    coord D = 2 * dx - dy;
    coord x = x1;

    for (coord y = y1; y <= y2; ++y) {
      positions_output.push_back({x, y});
      pixels_count_output++;
      if (D > 0) {
        x += xi;
        D -= 2 * dy;
      }
      D += 2 * dx;
    }
  };

  if (abs(y2 - y1) < abs(x2 - x1)) {
    if (x1 > x2) {
      lineX(x2, y2, x1, y1);
    } else {
      lineX(x1, y1, x2, y2);
    }
  } else {
    if (y1 > y2) {
      lineY(x2, y2, x1, y1);
    } else {
      lineY(x1, y1, x2, y2);
    }
  }
}

signed_distance algorithmes::calc_distance(simple_drawing::point from,
                                           simple_drawing::point to) {
  auto value =
      signed_distance(sqrt(pow((from.x - to.x), 2) + pow((from.y - to.y), 2)));
  return value > 0 ? value : 0;
}

void algorithmes::cirle_positions(
    point c, distance r, std::vector<simple_drawing::point>& v_output,
    simple_drawing::distance& pixel_count_output) {
  coord x = 0;
  coord y = coord(r);
  coord delta = 1 - 2 * coord(r);
  coord error = 0;
  while (y >= 0) {
    v_output.push_back(point{c.x + x, c.y + y});
    v_output.push_back(point{c.x + x, c.y - y});
    v_output.push_back(point{c.x - x, c.y + y});
    v_output.push_back(point{c.x - x, c.y - y});
    pixel_count_output += 4;
    error = 2 * (delta + y) - 1;
    if ((delta < 0) && (error <= 0)) {
      delta += 2 * ++x + 1;
      continue;
    }
    if ((delta > 0) && (error > 0)) {
      delta -= 2 * --y + 1;
      continue;
    }
    delta += 2 * (++x - y--);
  }
}

}  // namespace simple_drawing
