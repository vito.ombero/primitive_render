/*
 * algorytms.hxx
 *
 *  Created on: Oct 5, 2018
 *      Author: vito
 */

#ifndef ALGORYTMS_HXX_
#define ALGORYTMS_HXX_

#include <vector>

#include "sd_types.hpp"

namespace simple_drawing {

struct algorithmes {
  static point make_remotest_point(point a, point b, point c,
                                   bool max /*or min?*/);

  static void line_positions(point p1, point p2,
                             std::vector<point>& positions_output,
                             distance& pixels_count_output);

  static signed_distance calc_distance(point from, point to);

  static void cirle_positions(point c, distance r, std::vector<point>& v_output,
                              distance& pixel_count_output);

  template <typename T>  // NOTE: where T is integral
  static inline coord coords_to_index(T x, T y, distance w, distance h) {
    if ((x < 0) || (y < 0)) return 0;  // cartesian Q1
    coord temp = x + y * w;
    if (distance(temp) < h * w)
      return temp;
    else
      return 0;
  }
};

}  // namespace simple_drawing

#endif /* ALGORYTMS_HXX_ */
