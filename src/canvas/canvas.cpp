#include <array>

#include "canvas.hpp"
#include "color_map.hpp"

#include "sd_types.hpp"

namespace simple_drawing {

primitive_canvas::primitive_canvas(distance height, distance width,
                                   color background) {
  pixel_buffer_ptr = new pixel_buffer(width, height, background);
}

primitive_canvas::~primitive_canvas() { delete pixel_buffer_ptr; }

int_least8_t primitive_canvas::draw(drawable_primitive& primitive_) {
  if (pixel_buffer_ptr != nullptr) {
    assert(primitive_.get_right_lower_offset().y < coord(get_height()) &&
           primitive_.get_right_lower_offset().x < coord(get_width()));

    signed_distance prim_W = primitive_.get_right_lower_offset().x -
                             primitive_.get_left_upper_offset().x + 1;
    assert(prim_W > 0);
    assert(distance(prim_W) <= get_width());

    signed_distance prim_H = primitive_.get_right_lower_offset().y -
                             primitive_.get_left_upper_offset().y + 1;
    assert(prim_H > 0);
    assert(distance(prim_H) <= get_height());

    pixel_buffer_ptr->update(primitive_, prim_W, prim_H);

    return 1;
  }
  return 0;
}

int_least8_t primitive_canvas::erase() {
  distance width = get_width();
  distance height = get_height();
  delete pixel_buffer_ptr;
  pixel_buffer_ptr = new pixel_buffer(width, height);
  return 0;
}

distance primitive_canvas::get_height() const {
  return pixel_buffer_ptr->get_height();
}

distance primitive_canvas::get_width() const {
  return pixel_buffer_ptr->get_width();
}

int_least8_t primitive_canvas::applyShader(algorithm g, drawable_primitive& p) {
  (g(p));
  return 1;
}

int_least8_t primitive_canvas::applyShader(vertex_algorithm_c v,
                                           std::vector<pvertex>& vv,
                                           color color_fill) {
  (v(vv, *pixel_buffer_ptr->pixels, get_width(), get_height(), color_fill));
  return 0;
}
int_least8_t primitive_canvas::applyShader(vertex_algorithm v,
                                           std::vector<pvertex>& vv) {
  (v(vv, *pixel_buffer_ptr->pixels, get_width(), get_height()));
  return 0;
}

}  // namespace simple_drawing
