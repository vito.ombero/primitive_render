#ifndef COLOR_MAP_HPP
#define COLOR_MAP_HPP

#include <algorithm>
#include <cassert>
#include <map>

#include "rgb_color.hpp"
#include "sd_types.hpp"

namespace simple_drawing {

static std::map<simple_drawing::color, rgb_color> color_map = {
    {color::Black, rgb_color(0, 0, 0)},
    {color::Navy, rgb_color(0, 0, 128)},
    {color::Blue, rgb_color(0, 0, 255)},
    {color::Green, rgb_color(0, 128, 0)},
    {color::Teal, rgb_color(0, 128, 128)},
    {color::Blue_light, rgb_color(0, 128, 255)},
    {color::Lime, rgb_color{0, 255, 0}},
    {color::Green_springB, rgb_color{0, 255, 128}},
    {color::Cyan, rgb_color{0, 255, 255}},
    {color::Indigo, rgb_color{75, 0, 130}},
    {color::Maroon, rgb_color{128, 0, 0}},
    {color::Purple, rgb_color{128, 0, 128}},
    {color::Purple_blue, rgb_color{128, 0, 255}},
    {color::Olive, rgb_color{128, 128, 0}},
    {color::Gray, rgb_color{128, 128, 128}},
    {color::Sky_blue_dark, rgb_color{128, 128, 255}},
    {color::Silver, rgb_color{192, 192, 192}},
    {color::Violet, rgb_color{238, 130, 238}},
    {color::Red, rgb_color{255, 0, 0}},
    {color::Fuchsia_dark, rgb_color{255, 0, 128}},
    {color::Magenta, rgb_color{255, 0, 255}},
    {color::DarkOrange1B, rgb_color{255, 128, 0}},
    {color::Orange, rgb_color{255, 165, 0}},
    {color::Coral_lightd, rgb_color{255, 128, 128}},
    {color::Orchid1_l, rgb_color{255, 128, 255}},
    {color::Yellow, rgb_color{255, 255, 0}},
    {color::Khaki1_l, rgb_color{255, 255, 128}},
    {color::White, rgb_color{255, 255, 255}}};

typedef std::map<color, rgb_color> cmap;

}  // namespace simple_drawing
#endif  // COLOR_MAP_HPP
