/*
 * vshaders.hxx
 *
 *  Created on: Oct 6, 2018
 *      Author: vito
 */

#ifndef VSHADERS_HXX_
#define VSHADERS_HXX_

#include <math.h>
#include <algorithm>
#include <cassert>

#include "sd_types.hpp"

#include "drawable_primitive.hpp"
#include "rgb_color.hpp"

#include "algorithmes.hpp"
#include "color_map.hpp"
#include "triangle.hpp"

/// TODO: wrong idea and model of shader - remake it in opengl way
// it must be something like
// std::vector<var> vshader(std::vector<attributes>)
// struct atribute { float _1;float _2;float _3;float _4;}

namespace simple_drawing {
namespace shaders {
namespace vertex {

struct borders {
  std::vector<point> left_border;
  std::vector<point> right_border;
};

borders get_borders(std::vector<pvertex>& v);

// fill triangle with lines
// NOTE: in fact all lines are horizontal, so bresenham's line algorithm is
// too complex for this task
void fill_color_3v(std::vector<pvertex>& v /*in*/,
                   std::vector<rgb_color>& pixbuf /*out*/, distance w,
                   distance h, color fill_color);

// work with pixels - invert color inside tringle border
void invert_color_3v(std::vector<pvertex>& v /*in*/,
                     std::vector<rgb_color>& pixbuf /*out*/, distance w,
                     distance h);

}  // namespace vertex
}  // namespace shaders
}  // namespace simple_drawing

#endif /* VSHADERS_HXX_ */
