#ifndef PIXEL_BUFFER_HPP
#define PIXEL_BUFFER_HPP

#include <vector>

#include "drawable_primitive.hpp"
#include "sd_types.hpp"

namespace simple_drawing {

class pixel_buffer final {
 public:
  pixel_buffer(distance, distance, color = color::White);
  ~pixel_buffer();

  int_least8_t update(drawable_primitive&, signed_distance, signed_distance);

  distance get_height() const;
  distance get_width() const;

  // private:
  signed_distance h;
  signed_distance w;
  std::vector<rgb_color>* pixels;
};

}  // namespace simple_drawing
#endif  // PIXEL_BUFFER_HPP
