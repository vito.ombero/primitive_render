/*
 * pixel.cxx
 *
 *  Created on: Oct 6, 2018
 *      Author: vito
 */
#include "rgb_color.hpp"
#include "sd_types.hpp"

namespace simple_drawing {

rgb_color::rgb_color() {
  r = 255;
  g = 255;
  b = 255;
}

rgb_color::rgb_color(byte val) {
  r = val;
  g = val;
  b = val;
}

rgb_color::rgb_color(byte r_, byte g_, byte b_) {
  r = r_;
  g = g_;
  b = b_;
}

/// need for using color map
bool rgb_color::operator==(const rgb_color& b) const {
  return (this->r == b.r && this->g == b.g && this->b == b.b);
}

/// need for shaders
bool rgb_color::operator!=(const rgb_color& b) const {
  return (this->r != b.r && this->g != b.g && this->b != b.b);
}

}  // namespace simple_drawing
