#include "shaders_primitive.hpp"

namespace simple_drawing {
namespace shaders {
namespace primitive {

void invert_border_color(drawable_primitive& p) {
  rgb_color previous_col = color_map.find(p.getColor())->second;
  previous_col.b = (255 - previous_col.b);
  previous_col.r = (255 - previous_col.r);
  previous_col.g = (255 - previous_col.g);

  auto findResult = std::find_if(color_map.begin(), color_map.end(),
                                 [&](const std::pair<color, rgb_color>& pair) {
                                   return pair.second == previous_col;
                                 });

  if (findResult != std::end(color_map)) {
    p.setColor(findResult->first);
  } else {
    p.setColor(color::No);
  }
}

}  // namespace primitive
}  // namespace shaders
}  // namespace simple_drawing
