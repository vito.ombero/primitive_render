#ifndef PIXEL_HXX_
#define PIXEL_HXX_

#include "sd_types.hpp"

namespace simple_drawing {

struct rgb_color final {
  byte r;
  byte g;
  byte b;

  rgb_color();

  rgb_color(byte val);

  rgb_color(byte r_, byte g_, byte b_);

  bool operator==(const rgb_color& b) const;

  bool operator!=(const rgb_color& b) const;
};

}  // namespace simple_drawing
#endif /* PIXEL_HXX_ */
