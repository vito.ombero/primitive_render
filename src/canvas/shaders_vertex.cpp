#include "shaders_vertex.hpp"
#include "algorithmes.hpp"

namespace simple_drawing {
namespace shaders {
namespace vertex {

borders get_borders(std::vector<pvertex>& v) {
  // TODO: we need 3 veteces no need in vector<pvector>
  v.shrink_to_fit();
  assert(v.size() == 3);

  point max{algorithmes::make_remotest_point(v[0], v[1], v[2], true)};
  point min{algorithmes::make_remotest_point(v[0], v[1], v[2], false)};

  // detect_case
  auto LD = new std::vector<point>;
  auto RD = new std::vector<point>;
  distance countL{0};  // todo: make new overload
  distance countR{0};

  // imagine all possible triangles with 3 vertices
  // & collect info about left and right sides

  // filling : y_start > y_end
  if (v[0].y == v[2].y && v[1].y == min.y) {
    algorithmes::line_positions(v[0], v[1], *LD, countL);
    algorithmes::line_positions(v[2], v[1], *RD, countR);
  } else if (v[0].y == v[2].y && v[1].y == max.y) {
    algorithmes::line_positions(v[0], v[2], *LD, countL);
    algorithmes::line_positions(v[1], v[2], *RD, countR);
  } else if (v[0].x == min.x && v[1].x == v[2].x) {  // undefined direction
    if (v[1].y < v[2].y) {
      algorithmes::line_positions(v[2], v[0], *LD, countL);
      algorithmes::line_positions(v[0], v[1], *LD, countL);
      algorithmes::line_positions(v[2], v[1], *RD, countR);
    } else {
      algorithmes::line_positions(v[1], v[0], *LD, countL);
      algorithmes::line_positions(v[0], v[2], *LD, countL);
      algorithmes::line_positions(v[1], v[2], *RD, countR);
    }
  } else if (v[2].x == max.x && v[0].x == v[1].x) {  // undefined direction
    if (v[1].y > v[0].y) {
      algorithmes::line_positions(v[1], v[0], *LD, countL);
      algorithmes::line_positions(v[1], v[2], *RD, countR);
      algorithmes::line_positions(v[2], v[0], *RD, countR);
    } else {
      algorithmes::line_positions(v[0], v[1], *LD, countL);
      algorithmes::line_positions(v[0], v[2], *RD, countR);
      algorithmes::line_positions(v[2], v[1], *RD, countR);
    }
  } else if (v[2].y == max.y) {
    algorithmes::line_positions(v[2], v[0], *LD, countL);
    algorithmes::line_positions(v[0], v[1], *LD, countL);
    algorithmes::line_positions(v[2], v[1], *RD, countR);
  } else if (v[0].y == max.y) {
    algorithmes::line_positions(v[0], v[1], *LD, countL);
    algorithmes::line_positions(v[0], v[2], *RD, countR);
    algorithmes::line_positions(v[2], v[1], *RD, countR);
  }

  auto distinct_and_sort_by_y_coord = [&](std::vector<point>* bigger_side) {
    auto iter =
        std::unique(bigger_side->begin(), bigger_side->end(),
                    [](point a, point b) -> bool { return a.y == b.y; });
    bigger_side->erase(iter, bigger_side->end());
    bigger_side->shrink_to_fit();
    std::sort(bigger_side->begin(), bigger_side->end(),
              [](point a, point b) -> bool { return a.y < b.y; });
  };

  distinct_and_sort_by_y_coord(RD);
  distinct_and_sort_by_y_coord(LD);

  return borders{*LD, *RD};
}

// fill triangle with lines
// NOTE: in fact all lines are horizontal, so bresenham's line algorithm is too
// complex for this task
void fill_color_3v(std::vector<pvertex>& v /*in*/,
                   std::vector<rgb_color>& pixbuf /*out*/, distance w,
                   distance h, color fill_color) {
  auto borders = get_borders(v);

  auto LD = &borders.left_border;
  auto RD = &borders.right_border;

  // because of bresenham's line algorithm precision one of them is slightly
  // bigger
  auto count = LD->size() > RD->size() ? RD->size() : LD->size();
  // filling horizontal line by line
  for (ulong i = 0; i < count - 1; ++i) {
    point left = LD->at(i);
    point right = RD->at(i);
    // line we need
    if (left.y == right.y)
      for (int j = left.x + 1; j < right.x; ++j) {
        uint coords = q1_coord(algorithmes::coords_to_index(j, left.y, w, h));
        pixbuf[coords] = color_map[fill_color];
      }
  }
}

// work with pixels - invert color inside tringle border
void invert_color_3v(std::vector<pvertex>& v /*in*/,
                     std::vector<rgb_color>& pixbuf /*out*/, distance w,
                     distance h) {
  auto borders = get_borders(v);

  auto LD = &borders.left_border;
  auto RD = &borders.right_border;

  // because of bresenham's line algorithm precision one of them is slightly
  // bigger
  auto count = LD->size() > RD->size() ? RD->size() : LD->size();
  // filling horizontal line by line
  for (ulong i = 0; i < count - 1; ++i) {
    point left = LD->at(i);
    point right = RD->at(i);
    // line we need
    if (left.y == right.y)
      for (int j = left.x + 1; j < right.x; ++j) {
        uint coords = q1_coord(algorithmes::coords_to_index(j, left.y, w, h));
        rgb_color probe = pixbuf[coords];
        pixbuf[coords] = rgb_color{byte(255 - probe.r), byte(255 - probe.g),
                                   byte(255 - probe.b)};
      }
  }
}

}  // namespace vertex
}  // namespace shaders
}  // namespace simple_drawing
